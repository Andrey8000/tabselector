package crocodile8000.tabselector.lib;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import crocodile8000.tabselector.R;


public class MyTabsSelector extends HorizontalScrollView {

    private LinearLayout homeLayout;
    private List<OneTab> tabs;
    private int selectedNum;
    private TabsSelectListener tabsSelectListener;
    private boolean requestCheckBordersAfterLayout;



    public MyTabsSelector(Context context) {
        super(context);
        init();
    }

    public MyTabsSelector(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTabsSelector(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }



    private void init(){
        setHorizontalScrollBarEnabled(false);
        homeLayout = new LinearLayout(getContext());
        homeLayout.setOrientation(LinearLayout.HORIZONTAL);
        homeLayout.setGravity(Gravity.CENTER_VERTICAL);
        addView(homeLayout);
        tabs = new ArrayList<OneTab>();
    }



    /** Установить TabsSelectListener */
    public void setTabsSelectListener(TabsSelectListener listener){
        tabsSelectListener = listener;
    }



    /** Добавить таб с текстом */
    public void addTab(String text){
        OneTab oneTab = new OneTab(text, getContext());
        tabs.add(oneTab);
        homeLayout.addView(oneTab.textView);
        requestLayout();

        if (tabs.size()==1) oneTab.setSelected(true);

        final int num = tabs.size()-1;

        oneTab.textView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelectedTabNum(num);
            }
        });

    }



    /** Проверяем, находится ли в видимой области выбранный таб и, если надо, проматываем */
    public void checkForBorders(){
        TextView textView = tabs.get(selectedNum).textView;
        int textX = (int) textView.getX();

        // если ширина корневого скролла == 0, значит, скорее всего,
        // этот метод вызывается до onLayout и следует вызвать его после
        if (getWidth()==0) {
            requestCheckBordersAfterLayout = true;
            return;
        }

        if (textX + textView.getWidth() > getWidth()+getScrollX() ) {
            smoothScrollTo(textX, 0);
        }
        else if (textX < getScrollX()) {
            int targetX = selectedNum==0? textX : textX-tabs.get(selectedNum-1).textView.getWidth()/2;
            smoothScrollTo(targetX, 0);
        }
    }



    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        if (requestCheckBordersAfterLayout){
            requestCheckBordersAfterLayout = false;
            checkForBorders();
        }
    }



    /** Выделить таб по номеру */
    public void setSelectedTabNum(int num){
        selectedNum = num;
        for(int i=0; i<tabs.size(); i++){
            tabs.get(i).setSelected(i == num);
        }
        if (tabsSelectListener!=null) tabsSelectListener.onSelect(num);
        checkForBorders();
    }



    /** Удалить все табы */
    public void clearAllTabs(){
        homeLayout.removeAllViews();
        tabs.clear();
        selectedNum = 0;
        tabsSelectListener = null;
        requestLayout();
    }





    public static class OneTab{

        public TextView textView;
        public String text;

        public OneTab(String text, Context context) {
            this.text = text;
            textView = new TextView(context);
            textView.setText("    "+text+"    ");
            textView.setGravity(Gravity.CENTER);
            textView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT));

            textView.setBackgroundResource(R.drawable.tab_top_selected); // можно доделать setter что бы устанавливать извне
            textView.setBackgroundColor(Color.TRANSPARENT);
        }

        public void setSelected(boolean selected){
            if (selected) textView.setBackgroundResource(R.drawable.tab_top_selected);
            else textView.setBackgroundColor(Color.TRANSPARENT);
        }
    }






    public static interface TabsSelectListener {

        public void onSelect(int num);

    }
}
