package crocodile8000.tabselector;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import crocodile8000.tabselector.lib.MyTabsSelector;


public class MainActivity extends Activity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initTabs();
    }



    /** test */
    private void initTabs() {

        MyTabsSelector myTabsSelector = (MyTabsSelector) findViewById(R.id.myTabsSelector);
        myTabsSelector.clearAllTabs();

        for(int i=0; i<20; i++)
        myTabsSelector.addTab("Tab num "+i);

        final TextView tv1 = (TextView) findViewById(R.id.tv1);

        myTabsSelector.setTabsSelectListener(new MyTabsSelector.TabsSelectListener() {
            @Override
            public void onSelect(int num) {
                tv1.setText("onSelect: " + num);
            }
        });

    }
}
